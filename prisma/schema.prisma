generator client {
  provider = "prisma-client-js"
}

datasource db {
  provider = "mysql"
  url      = env("DATABASE_URL")
}

model category {
  id          Int       @id @default(autoincrement())
  name        String?   @db.VarChar(100)
  description String?   @db.LongText
  created_at  DateTime? @db.DateTime(0) @default(now())
  updated_at  DateTime? @db.DateTime(0)
  product     product[]
}

model feedback {
  id         Int       @id @default(autoincrement())
  user_id    Int?
  product_id Int?
  rating     Int?
  comment    String?   @db.VarChar(1000)
  status     Int?      @default(0)
  created_at DateTime? @db.DateTime(0) @default(now())
  updated_at DateTime? @db.DateTime(0)
  user       user?     @relation(fields: [user_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "feedback_ibfk_1")
  product    product?  @relation(fields: [product_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "feedback_ibfk_2")

  @@index([product_id], map: "product_id")
  @@index([user_id], map: "user_id")
}

model galery {
  id         Int      @id @default(autoincrement())
  product_id Int?
  thumbnail  String?  @db.VarChar(500)
  product    product? @relation(fields: [product_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "galery_ibfk_1")
created_at     DateTime?   @db.DateTime(0) @default(now())
  updated_at     DateTime?   @db.DateTime(0)
  @@index([product_id], map: "product_id")
}

model product {
  id             Int         @id @default(autoincrement())
  category_id    Int?
  store_id       Int?
  title          String?     @db.VarChar(250)
  price          Int?
  discount_type  String?     @db.VarChar(50)
  discount_value Int?
  thumbnail      String?     @db.VarChar(500)
  description    String?     @db.LongText
  created_at     DateTime?   @db.DateTime(0) @default(now())
  updated_at     DateTime?   @db.DateTime(0)
  cart_item      cart_item[]
  feedback       feedback[]
  galery         galery[]
  category       category?   @relation(fields: [category_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "product_ibfk_1")
  store          store?      @relation(fields: [store_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "product_ibfk_2")

  @@index([category_id], map: "category_id")
  @@index([store_id], map: "store_id")
}

model store {
  id          Int       @id @default(autoincrement())
  user_id     Int?
  name        String?   @db.VarChar(50)
  description String?   @db.LongText
  type        String?   @db.VarChar(50)
  address     String?   @db.VarChar(500)
  city        String?   @db.VarChar(50)
  country     String?   @db.VarChar(30)
  created_at  DateTime? @db.DateTime(0) @default(now())
  updated_at  DateTime? @db.DateTime(0)
  product     product[]
  user        user?     @relation(fields: [user_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "store_ibfk_1")

  @@index([user_id], map: "user_id")
}

model user {
  id                    Int        @id @default(autoincrement())
  firstname             String?    @db.VarChar(50)
  lastname              String?    @db.VarChar(50)
  store_id              Int?
  phone_number_or_email String?    @db.VarChar(20)
  address               String?    @db.VarChar(200)
  password              String?    @db.VarChar(200)
  role                  String?    @db.VarChar(10)
  created_at            DateTime?  @db.DateTime(0) @default(now())
  updated_at            DateTime?  @db.DateTime(0)
  cart                  cart?
  feedback              feedback[]
  order                 order[]
  payment               payment[]
  store                 store[]
  token                 token[]
}

model token {
  id         Int       @id @default(autoincrement())
  user_id    Int?
  token      String?   @db.VarChar(500)
  created_at DateTime? @db.DateTime(0) @default(now())
  user       user?     @relation(fields: [user_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "token_ibfk_1")

  @@index([user_id], map: "user_id")
}

model cart {
  id            Int         @id @default(autoincrement())
  user_id       Int?
  shop_order_id Int?
  created_at    DateTime?   @db.DateTime(0) @default(now())
  updated_at    DateTime?   @db.DateTime(0)
  user          user        @relation(fields: [id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "cart_ibfk_1")
  order         order?      @relation(fields: [shop_order_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "cart_ibfk_2")
  cart_item     cart_item[]

  @@index([shop_order_id], map: "shop_order_id")
}

model cart_item {
  id         Int       @id @default(autoincrement())
  cart_id    Int?
  product_id Int?
  quantity   Int?
  created_at DateTime? @db.DateTime(0) @default(now())
  updated_at DateTime? @db.DateTime(0)
  cart       cart?     @relation(fields: [cart_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "cart_item_ibfk_1")
  product    product?  @relation(fields: [product_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "cart_item_ibfk_2")

  @@index([cart_id], map: "cart_id")
  @@index([product_id], map: "product_id")
}

model order {
  id           Int       @id @default(autoincrement())
  user_id      Int?
  fullname     String?   @db.VarChar(50)
  email        String?   @db.VarChar(150)
  phone_number String?   @db.VarChar(20)
  address      String?   @db.VarChar(200)
  note         String?   @db.VarChar(1000)
  order_date   DateTime? @db.DateTime(0)
  status       Int?
  payment_id   Int?
  total_money  Int?
  shipping_id  Int?
  created_at   DateTime? @db.DateTime(0) @default(now())
  updated_at   DateTime? @db.DateTime(0)
  cart         cart[]
  user         user?     @relation(fields: [user_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "order_ibfk_1")
  payment      payment?  @relation(fields: [payment_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "order_ibfk_2")
  shipping     shipping? @relation(fields: [shipping_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "order_ibfk_3")

  @@index([payment_id], map: "payment_id")
  @@index([shipping_id], map: "shipping_id")
  @@index([user_id], map: "user_id")
}

model payment {
  id             Int       @id @default(autoincrement())
  user_id        Int?
  date_payment   DateTime? @db.DateTime(0)
  method_payment String?   @db.VarChar(50)
  card_id        String?   @db.VarChar(50)
  last4          String?   @db.VarChar(50)
  brand          String?   @db.VarChar(50)
  created_at     DateTime? @db.DateTime(0) @default(now())
  updated_at     DateTime? @db.DateTime(0)
  order          order[]
  user           user?     @relation(fields: [user_id], references: [id], onDelete: NoAction, onUpdate: NoAction, map: "payment_ibfk_1")

  @@index([user_id], map: "user_id")
}

model shipping {
  id         Int       @id @default(autoincrement())
  name       String?   @db.VarChar(50)
  price      Int?
  created_at DateTime? @db.DateTime(0) @default(now())
  updated_at DateTime? @db.DateTime(0)
  order      order[]
}
