import { PrismaClient, product } from '@prisma/client';

const prisma = new PrismaClient();
async function main() {
  const countDataCategory = await prisma.category.count();
  const countDataUser = await prisma.user.count();
  const countDataProduct = await prisma.product.count();
  if (countDataUser === 0) {
    await prisma.user.createMany({
      data: [
        {
          firstname: 'admin nè',
          lastname: 'admin nè',
          phone_number_or_email: 'admin@gmail.com',
          address: 'hà nội',
          password:
            '$2b$10$zAgdRObHq9uKlWfeY1LxAe6aVThRdIC3/WzYIdQarhMfGaVFQUnR6',
          role: 'admin',
        },
        {
          firstname: 'duyanh',
          lastname: 'tong',
          phone_number_or_email: 'duyanh@gmail.com',
          address: 'hà nội',
          password:
            '$2b$10$zAgdRObHq9uKlWfeY1LxAe6aVThRdIC3/WzYIdQarhMfGaVFQUnR6',
          role: 'user',
        },
        {
          firstname: 'shop1 nè',
          lastname: 'shop nè',
          phone_number_or_email: 'shop@gmail.com',
          address: 'hà nội',
          password:
            '$2b$10$zAgdRObHq9uKlWfeY1LxAe6aVThRdIC3/WzYIdQarhMfGaVFQUnR6',
          role: 'shop',
        },
      ],
    });
  }
  if (countDataCategory === 0) {
    await prisma.category.createMany({
      data: [
        {
          name: 'Thức ăn thú cưng',
          description: 'Thức ăn cho thú cưng',
        },
        {
          name: 'Rau xanh',
          description: 'Rau xanh',
        },
        {
          name: 'Thịt Heo',
          description: 'Thịt Heo',
        },
        {
          name: 'Hoa Quả',
          description: 'Hoa Quả',
        },
      ],
    });
  }
}

try {
  main();
  console.log('Successful');
} catch (error) {
  console.error(error);
  process.exit(1);
}
