export interface StoreFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
