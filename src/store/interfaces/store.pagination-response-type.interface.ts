import { store } from '@prisma/client';

export interface StorePaginationResponseType {
  data: store[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
