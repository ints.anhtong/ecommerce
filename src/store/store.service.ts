import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { CreateStoreDto } from './dtos/store.create.dto';
import { store } from '@prisma/client';
import { UpdateStoreDto } from './dtos/store.update.dto';
import { StoreFilterType } from './interfaces/store.filter-type.interface';
import { StorePaginationResponseType } from './interfaces/store.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class StoreService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(storeData: CreateStoreDto): Promise<store> {
    const store = await this.prismaService.store.findFirst({
      where: { name: storeData.name },
    });
    if (store) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store is exist',
      });
    }
    const user = await this.prismaService.user.findFirst({
      where: { id: storeData.user_id },
    });
    if (!user || user.store_id != null) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found or created a store',
      });
    }
    await this.prismaService.user.update({
      where: { id: user.id },
      data: {
        role: 'store',
      },
    });
    const storeNew = await this.prismaService.store.create({
      data: { ...storeData },
    });
    await this.prismaService.user.update({
      where: { id: storeData.user_id },
      data: { store_id: storeNew.id },
    });

    return storeNew;
  }

  async update(id: number, storeData: UpdateStoreDto): Promise<store> {
    const store = await this.prismaService.store.findFirst({
      where: { id: id },
    });
    if (!store) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store does not exist',
      });
    }
    const checkNameStore = await this.prismaService.store.findFirst({
      where: { name: storeData.name },
    });
    if (checkNameStore) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'store name is exist',
      });
    }
    const user = await this.prismaService.user.findFirst({
      where: { id: storeData.user_id },
    });
    if (!user) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found',
      });
    }
    return await this.prismaService.store.update({
      where: { id: id },
      data: { ...storeData, updated_at: new Date() },
    });
  }

  async getAll(filter: StoreFilterType): Promise<StorePaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;

    const stores = await this.prismaService.store.findMany({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          {
            name: { contains: search },
          },
          { description: { contains: search } },
          { country: { contains: search } },
          { address: { contains: search } },
          { city: { contains: search } },
        ],
      },
      orderBy: { created_at: 'desc' },
    });
    const total = await this.prismaService.store.count({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          {
            name: { contains: search },
          },
          { description: { contains: search } },
          { country: { contains: search } },
          { address: { contains: search } },
          { city: { contains: search } },
        ],
      },
      orderBy: { created_at: 'desc' },
    });

    return {
      data: stores,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }

  async getDetail(id: number): Promise<store> {
    const store = await this.prismaService.store.findFirst({
      where: { id: id },
    });
    if (!store) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'store not found',
      });
    }
    return store;
  }

  async delete(id: number): Promise<DeleteResponse<store>> {
    const store = await this.prismaService.store.findFirst({
      where: { id: id },
    });
    if (!store) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'store not found',
      });
    }
    await this.prismaService.store.delete({
      where: { id: store.id },
    });
    return {
      message: 'delete store successful',
      data: store,
    };
  }
}
