import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { StoreService } from './store.service';
import { CreateStoreDto } from './dtos/store.create.dto';
import { store } from '@prisma/client';
import { Roles } from 'src/auth/decorators/role.decorator';
import { UpdateStoreDto } from './dtos/store.update.dto';
import { StoreFilterType } from './interfaces/store.filter-type.interface';
import { StorePaginationResponseType } from './interfaces/store.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { ApiTags } from '@nestjs/swagger';
import {
  StoreCreateDoc,
  StoreDeleteDoc,
  StoreGetDetailDoc,
  StoreGetListDoc,
  StoreUpdateDoc,
} from './docs/store.doc';

@ApiTags('Store')
@Controller('store')
export class StoreController {
  constructor(private readonly storeService: StoreService) {}

  @StoreCreateDoc()
  @Post()
  @Roles('user', 'admin')
  create(@Body() body: CreateStoreDto): Promise<store> {
    return this.storeService.create(body);
  }

  @StoreUpdateDoc()
  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateStoreDto,
  ): Promise<store> {
    return this.storeService.update(id, body);
  }

  @StoreGetListDoc()
  @Get()
  @Roles('admin', 'store', 'admin')
  getAll(
    @Query() filter: StoreFilterType,
  ): Promise<StorePaginationResponseType> {
    return this.storeService.getAll(filter);
  }

  @StoreGetDetailDoc()
  @Get(':id')
  @Roles('admin', 'user', 'store')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<store> {
    return this.storeService.getDetail(id);
  }

  @StoreDeleteDoc()
  @Delete(':id')
  @Roles('admin')
  delete(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResponse<store>> {
    return this.storeService.delete(id);
  }
}
