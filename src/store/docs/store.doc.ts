import { applyDecorators } from '@nestjs/common';
import { ApiOperation, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import {
  StoreCreateSerialization,
  StoreDeleteSerialization,
  StoreGetDetailSerialization,
  StoreGetListSerialization,
  StoreUpdateSerialization,
} from '../serializations/store.serialization';
import { faker } from '@faker-js/faker';

export function StoreCreateDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module store' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreCreateSerialization,
    }),
  );
}

export function StoreUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.number.int(50),
      type: 'number',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreUpdateSerialization,
    }),
  );
}

export function StoreGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module store' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreGetListSerialization,
    }),
  );
}

export function StoreGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.number.int(50),
      type: 'number',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreGetDetailSerialization,
    }),
  );
}

export function StoreDeleteDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module store' }),
    ApiParam({
      name: 'id',
      example: faker.number.int(50),
      type: 'number',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: StoreDeleteSerialization,
    }),
  );
}
