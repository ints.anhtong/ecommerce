import { CreateStoreDto } from './store.create.dto';

export class UpdateStoreDto extends CreateStoreDto {}
