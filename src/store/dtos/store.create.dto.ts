import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional } from 'class-validator';

export class CreateStoreDto {
  @ApiProperty({
    example: faker.number.int(100),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  user_id: number;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  name: string;
  @ApiProperty({
    example: faker.word.words(5),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  description?: string;
  @ApiProperty({
    example: 'food',
    required: true,
  })
  @IsOptional()
  @Type(() => String)
  type?: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  address: string;
  @ApiProperty({
    example: faker.location.city(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  city: string;
  @ApiProperty({
    example: faker.location.county(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  country: string;
}
