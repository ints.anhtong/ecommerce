import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { StorePaginationResponseType } from '../interfaces/store.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { store } from '@prisma/client';

export class StoreCreateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly user_id: number;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
    nullable: false,
  })
  readonly name: string;
  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: 'food',
    required: true,
    nullable: false,
  })
  readonly type: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.location.city(),
    required: true,
    nullable: false,
  })
  readonly city: string;
  @ApiProperty({
    example: faker.location.country(),
    required: true,
    nullable: false,
  })
  readonly country: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
}

export class StoreUpdateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly user_id: number;
  @ApiProperty({
    example: faker.word.words(1),
    required: true,
    nullable: false,
  })
  readonly name: string;
  @ApiProperty({
    example: faker.word.words(5),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: 'food',
    required: true,
    nullable: false,
  })
  readonly type: string;
  @ApiProperty({
    example: faker.location.street(),
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.location.city(),
    required: true,
    nullable: false,
  })
  readonly city: string;
  @ApiProperty({
    example: faker.location.country(),
    required: true,
    nullable: false,
  })
  readonly country: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
}

export class StoreGetListSerialization implements StorePaginationResponseType {
  @ApiProperty({
    type: [StoreUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: StoreUpdateSerialization[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class StoreGetDetailSerialization extends StoreUpdateSerialization {}

export class StoreDeleteSerialization implements DeleteResponse<store> {
  @ApiProperty({
    example: 'delete category successful',
  })
  message: string;
  @ApiProperty({
    type: [StoreUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: StoreUpdateSerialization;
}
