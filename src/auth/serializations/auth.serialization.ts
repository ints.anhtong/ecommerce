import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';

export class AuthLoginSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.string.alphanumeric(30),
    description: 'Will be valid JWT Encode string',
    required: true,
    nullable: false,
  })
  readonly accessToken: string;
  @ApiProperty({
    example: faker.string.alphanumeric(30),
    description: 'Will be valid JWT Encode string',
    required: true,
    nullable: false,
  })
  readonly refreshToken: string;
}

export class AuthRegisterSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.person.firstName(),
    required: true,
    nullable: false,
  })
  readonly firstname: string;
  @ApiProperty({
    example: faker.person.lastName(),
    required: true,
    nullable: false,
  })
  readonly lastname: string;
  @ApiProperty({
    example: faker.internet.email(),
    required: true,
    nullable: false,
  })
  readonly phone_number_or_email: string;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly address: string;
  @ApiProperty({
    example: faker.internet.password(),
    required: true,
    nullable: false,
  })
  readonly password: string;
  @ApiProperty({
    example: 'user',
    required: true,
    nullable: false,
  })
  readonly role: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}
