import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { PrismaService } from 'src/prisma.service';
import { RegisterDto } from './dtos/auth.register.dto';
import { hash, compare } from 'bcrypt';
import { LoginDto } from './dtos/auth.login.dto';
import { user } from '@prisma/client';
import { AuthResponseLogin } from './interfaces/auth.response-login';

@Injectable()
export class AuthService {
  constructor(
    private readonly prismaService: PrismaService,
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
  ) {}
  async validateEmailOrPhoneNumber(emailOrPhone: string): Promise<void> {
    const emailRegex: RegExp = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
    const phoneRegex: RegExp = /^(0[1-9])+([0-9]{8,9})\b/;
    if (!(emailRegex.test(emailOrPhone) || phoneRegex.test(emailOrPhone))) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'phone or email is not valid',
      });
    }
  }

  register = async (userRegister: RegisterDto): Promise<user> => {
    //check validate email and phone number
    await this.validateEmailOrPhoneNumber(userRegister.emailOrPhone);
    // check compare password
    if (userRegister.password !== userRegister.rePassword) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'confirm password is not correct',
      });
    }

    //check exists user
    const user = await this.prismaService.user.findFirst({
      where: {
        phone_number_or_email: userRegister.emailOrPhone,
      },
    });
    if (user) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'this email(phone number) has been used',
      });
    }

    //hash password
    const hashPassword = await hash(userRegister.password, 10);

    return await this.prismaService.user.create({
      data: {
        firstname: userRegister.firstName,
        lastname: userRegister.lastName,
        phone_number_or_email: userRegister.emailOrPhone,
        password: hashPassword,
        role: 'user',
      },
    });
  };

  async login(userData: LoginDto): Promise<AuthResponseLogin> {
    //checking exists user
    const user = await this.prismaService.user.findFirst({
      where: {
        phone_number_or_email: userData.emailOrPhone,
      },
    });
    if (!user) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user does not exists',
      });
    }
    // check compare password
    const verifyPassword = await compare(userData.password, user.password);
    if (!verifyPassword) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'password is not correct',
      });
    }
    // generate accessToken and refreshToken
    const payload = {
      id: user.id,
      emailOrPhoneNumber: user.phone_number_or_email,
      role: user.role,
    };
    return await this.generateToken(payload);
  }

  private async generateToken(payload: {
    id: number;
    emailOrPhoneNumber: string;
    role: string;
  }) {
    try {
      const accessToken = await this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
        expiresIn: this.configService.get<string>('EXPIRESIN_ACCESS_TOKEN'),
      });
      const refreshToken = await this.jwtService.signAsync(payload, {
        secret: this.configService.get<string>('REFRESH_TOKEN_KEY'),
        expiresIn: this.configService.get<string>('EXPIRESIN_REFRESH_TOKEN'),
      });
      await this.prismaService.token.create({
        data: {
          user_id: payload.id,
          token: refreshToken,
        },
      });
      return { id: payload.id, accessToken, refreshToken };
    } catch (error) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'error generate Token',
      });
    }
  }
}
