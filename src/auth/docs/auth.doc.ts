import { applyDecorators } from '@nestjs/common';
import { ApiOperation, ApiResponse } from '@nestjs/swagger';
import {
  AuthLoginSerialization,
  AuthRegisterSerialization,
} from '../serializations/auth.serialization';

export function AuthLoginDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module auth' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: AuthLoginSerialization,
    }),
  );
}

export function AuthRegisterDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module auth' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: AuthRegisterSerialization,
    }),
  );
}
