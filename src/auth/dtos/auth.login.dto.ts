import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { IsNotEmpty, IsString } from 'class-validator';

export class LoginDto {
  @IsNotEmpty()
  @ApiProperty({
    example: faker.internet.email(),
    required: true,
  })
  emailOrPhone: string;
  @ApiProperty({
    example: faker.internet.password(),
    required: true,
  })
  @IsNotEmpty()
  password: string;
}
