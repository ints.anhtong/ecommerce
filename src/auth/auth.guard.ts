import {
  CanActivate,
  ExecutionContext,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Reflector } from '@nestjs/core';
import { JwtService } from '@nestjs/jwt';
import { extractTokenFromHeader } from 'src/common/helper/extractTokenFromHeader';


import { PrismaService } from 'src/prisma.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(
    private readonly jwtService: JwtService,
    private readonly configService: ConfigService,
    private readonly prismaService: PrismaService,
    private readonly reflector: Reflector,
  ) {}
  async canActivate(context: ExecutionContext): Promise<boolean> {
    const isPublic = this.reflector.getAllAndOverride<string[]>('isPublic', [
      context.getHandler(),
      context.getClass(),
    ]);

    if (isPublic) {
      return true;
    }

    const request = context.switchToHttp().getRequest();

    const token = await extractTokenFromHeader(request);

    if (!token) {
      throw new UnauthorizedException({ message: 'you can login' });
    }

    try {
      const payload = await this.jwtService.verifyAsync(token, {
        secret: this.configService.get<string>('ACCESS_TOKEN_KEY'),
      });

      //request['user_data'] = payload;
      const user = await this.prismaService.user.findFirst({
        where: {
          id: payload.id,
        },
      });

      if (!user) {
        throw new UnauthorizedException({ message: 'account does not exists' });
      }
      request['account'] = user;
    } catch (error) {
      throw new UnauthorizedException();
    }
    return true;
  }
}
