
export interface AuthResponseLogin{
    id:number,
    accessToken :string,
    refreshToken :string
}