import { Body, Controller, Post } from '@nestjs/common';
import { RegisterDto } from './dtos/auth.register.dto';
import { user } from '@prisma/client';
import { AuthService } from './auth.service';
import { LoginDto } from './dtos/auth.login.dto';
import { Public } from './decorators/public.decorator';
import { AuthResponseLogin } from './interfaces/auth.response-login';
import { ApiTags } from '@nestjs/swagger';
import { AuthLoginDoc, AuthRegisterDoc } from './docs/auth.doc';
@ApiTags('Auth')
@Controller('auth')
export class AuthController {
  constructor(private authService: AuthService) {}
  @AuthRegisterDoc()
  @Post('register')
  @Public()
  async register(@Body() body: RegisterDto): Promise<user> {
    return await this.authService.register(body);
  }
  @AuthLoginDoc()
  @Post('login')
  @Public()
  async login(@Body() body: LoginDto): Promise<AuthResponseLogin> {
    return await this.authService.login(body);
  }
}
