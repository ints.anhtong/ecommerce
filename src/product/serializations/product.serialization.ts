import { ApiProperty } from '@nestjs/swagger';
import { faker } from '@faker-js/faker';
import { ProductPaginationResponseType } from '../interfaces/product.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { product } from '@prisma/client';

export class ProductCreateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly category_id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly store_id: number;
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
    nullable: false,
  })
  readonly title: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly price: number;
  @ApiProperty({
    example: 'sales',
    required: true,
    nullable: false,
  })
  readonly discount_type: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly discount_value: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly thumbnail: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}

export class ProductUpdateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly category_id: number;
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly store_id: number;
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
    nullable: false,
  })
  readonly title: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly price: number;
  @ApiProperty({
    example: 'sales',
    required: true,
    nullable: false,
  })
  readonly discount_type: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
    nullable: false,
  })
  readonly discount_value: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: true,
    nullable: false,
  })
  readonly thumbnail: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly deleted: number;
}

export class ProductGetListSerialization
  implements ProductPaginationResponseType
{
  @ApiProperty({
    type: [ProductUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: ProductUpdateSerialization[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class ProductGetDetailSerialization extends ProductUpdateSerialization {}

export class ProductDeleteSerialization implements DeleteResponse<product> {
  @ApiProperty({
    example: 'delete product successful',
  })
  message: string;
  @ApiProperty({
    type: [ProductUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: ProductUpdateSerialization;
}
