import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsNumber, IsOptional, Min, min } from 'class-validator';

export class CreateProductDto {
  @ApiProperty({
    example: faker.commerce.productName(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  title: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  @Min(1000)
  price: number;
  @ApiProperty({
    example: faker.number.int(100),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  category_id: number;
  @ApiProperty({
    example: faker.number.int(100),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  store_id: number;
  @ApiProperty({
    example: 'sales',
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  discount_type?: string;
  @ApiProperty({
    example: faker.commerce.price(),
    required: false,
  })
  @IsOptional()
  @IsNumber()
  @Min(1000)
  discount_value?: number;
  @ApiProperty({
    example: faker.internet.url(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  thumbnail?: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: false,
  })
  @IsOptional()
  @Type(() => String)
  description?: string;
}
