import { CreateProductDto } from './product.create.dto';

export class UpdateProductDto extends CreateProductDto {}
