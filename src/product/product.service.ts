import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { product, store, category } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
import { CreateProductDto } from './dtos/product.create.dto';
import { UpdateProductDto } from './dtos/product.update.dto';
import { ProductFilterType } from './interfaces/product.filter-type.interface';
import { ProductPaginationResponseType } from './interfaces/product.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { fi } from '@faker-js/faker';
import { SORT_BY_PRODUCT } from './constants/product.sort-by.constants';

@Injectable()
export class ProductService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(productData: CreateProductDto): Promise<product> {
    const store = await this.prismaService.store.findFirst({
      where: { id: productData.store_id },
    });
    if (!store) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Store not found',
      });
    }
    const category = await this.prismaService.category.findFirst({
      where: { id: productData.category_id },
    });
    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'category not found',
      });
    }
    const product = await this.prismaService.product.findFirst({
      where: {
        title: productData.title,
        category_id: productData.category_id,
        store_id: productData.store_id,
      },
    });
    if (product) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'product is exist',
      });
    }
    return await this.prismaService.product.create({
      data: { ...productData },
    });
  }

  async update(id: number, productData: UpdateProductDto): Promise<product> {
    const product = await this.prismaService.product.findFirst({
      where: { id: id },
    });
    if (!product) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }
    return await this.prismaService.product.update({
      where: { id: id },
      data: { ...productData, updated_at: new Date() },
    });
  }

  async getList(
    filter: ProductFilterType,
  ): Promise<ProductPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    let order_by: 'asc' | 'desc' =
      filter.order_by?.toLowerCase() === 'desc' ? 'desc' : 'asc';

    const store_id: number = Number(filter.store_id);
    const category_id: number = Number(filter.category_id);
    const whereConditions: any = {
      OR: [
        {
          title: { contains: search },
        },
        { description: { contains: search } },
      ],
    };

    if (!isNaN(store_id)) {
      whereConditions.store_id = store_id;
    }
    if (!isNaN(category_id)) {
      whereConditions.category_id = category_id;
    }

    if (!SORT_BY_PRODUCT.includes(sort_by)) {
      sort_by = 'created_at';
    }
    const products = await this.prismaService.product.findMany({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
    });
    const total = await this.prismaService.product.count({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
    });
    return {
      data: products,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }

  async getDetail(id: number): Promise<product> {
    const product = await this.prismaService.product.findFirst({
      where: { id: id },
    });
    if (!product) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }
    return product;
  }

  async delete(id: number): Promise<DeleteResponse<product>> {
    const product = await this.prismaService.product.findFirst({
      where: { id: id },
    });
    if (!product) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product not found',
      });
    }
    await this.prismaService.product.delete({
      where: { id: id },
    });
    return {
      message: 'delete product successful',
      data: product,
    };
  }
}
