export const SORT_BY_PRODUCT: string[] = [
  'created_at',
  'title',
  'price',
  'discount_value',
];
