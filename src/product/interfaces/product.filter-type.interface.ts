import { category } from '@prisma/client';
export interface ProductFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  sort_by?: string;
  order_by?: string;
  category_id?: number;
  store_id?: number;
  
}
