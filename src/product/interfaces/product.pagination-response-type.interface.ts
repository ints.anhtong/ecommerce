import { product } from '@prisma/client';

export interface ProductPaginationResponseType {
  data: product[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
