import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { ProductService } from './product.service';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateProductDto } from './dtos/product.create.dto';
import { product } from '@prisma/client';
import { UpdateProductDto } from './dtos/product.update.dto';
import { ProductFilterType } from './interfaces/product.filter-type.interface';
import { ProductPaginationResponseType } from './interfaces/product.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { ApiTags } from '@nestjs/swagger';
import {
  ProductCreateDoc,
  ProductDeleteDoc,
  ProductGetDetailDoc,
  ProductGetListDoc,
  ProductUpdateDoc,

} from './docs/product.doc';

@ApiTags('Product')
@Controller('product')
export class ProductController {
  constructor(private readonly productService: ProductService) {}

  @ProductCreateDoc()
  @Post()
  @Roles('admin', 'store')
  create(@Body() body: CreateProductDto): Promise<product> {
    return this.productService.create(body);
  }

  @ProductUpdateDoc()
  @Put(':id')
  @Roles('store', 'admin')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateProductDto,
  ): Promise<product> {
    return this.productService.update(id, body);
  }

  @ProductGetListDoc()
  @Get()
  @Roles('store', 'admin', 'user')
  getAll(
    @Query() filter: ProductFilterType,
  ): Promise<ProductPaginationResponseType> {
    return this.productService.getList(filter);
  }

  @ProductGetDetailDoc()
  @Get(':id')
  @Roles('store', 'admin', 'user')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<product> {
    return this.productService.getDetail(id);
  }

  @ProductDeleteDoc()
  @Delete(':id')
  @Roles('admin', 'store')
  delete(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResponse<product>> {
    return this.productService.delete(id);
  }
}
