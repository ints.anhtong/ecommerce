import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { UpdateUserDto } from './user/dtos/user.update.dto';
import { user } from '@prisma/client';

@Injectable()
export class AppService {}
