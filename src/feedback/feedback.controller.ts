import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { FeedbackService } from './feedback.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { CreateFeedBackDto } from './dtos/feedback.create.dto';
import { feedback } from '@prisma/client';
import { UpdateFeedBackDto } from './dtos/feedback.update.dto';
import { FeedBackFilterType } from './interfaces/feedback.filter-type.interface';
import { FeedBackPaginationResponseType } from './interfaces/feedback.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@ApiTags('Feedback')
@Controller('feedback')
export class FeedbackController {
  constructor(private readonly feedbackService: FeedbackService) {}

  @Post()
  @Roles('admin', 'user', 'store')
  create(@Body() body: CreateFeedBackDto): Promise<feedback> {
    return this.feedbackService.create(body);
  }

  @Put(':id')
  @Roles('admin', 'user', 'store')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateFeedBackDto,
  ): Promise<feedback> {
    return this.feedbackService.update(id, body);
  }

  @Get()
  @Roles('admin', 'user', 'store')
  getAll(
    @Query() filter: FeedBackFilterType,
  ): Promise<FeedBackPaginationResponseType> {
    return this.feedbackService.getList(filter);
  }

  @Get(':id')
  @Roles('store', 'admin', 'user')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<feedback> {
    return this.feedbackService.getDetail(id);
  }

  @Delete(':id')
  @Roles('admin', 'store')
  delete(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResponse<feedback>> {
    return this.feedbackService.delete(id);
  }
}
