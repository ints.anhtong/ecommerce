export interface FeedBackFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
  sort_by?: string;
  order_by?: string;
  user_id?: number;
  product_id?: number;
  rating?: number;
}
