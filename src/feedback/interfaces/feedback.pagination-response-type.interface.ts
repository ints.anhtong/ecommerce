import { feedback } from '@prisma/client';

export interface FeedBackPaginationResponseType {
  data: feedback[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
