import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';

import {
  IsNotEmpty,
  IsNumber,
  IsOptional,
  Max,
  Min,
  min,
} from 'class-validator';

export class CreateFeedBackDto {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  user_id: number;

  @ApiProperty({
    example: faker.number.int(50),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  product_id: number;

  @ApiProperty({
    example: faker.number.int(5),
    required: true,
  })
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Min(5)
  rating: number;
  @ApiProperty({
    example: faker.word.words(5),
    required: true,
  })
  @IsOptional()
  comment?: string;
  @IsNotEmpty()
  @IsNumber()
  @Min(0)
  @Max(2)
  @ApiProperty({
    example: faker.number.int(2),
    required: true,
  })
  status: number;
}
