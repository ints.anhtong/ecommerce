import { CreateFeedBackDto } from './feedback.create.dto';

export class UpdateFeedBackDto extends CreateFeedBackDto {}
