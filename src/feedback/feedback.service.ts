import { HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { feedback, product } from '@prisma/client';
import { CreateFeedBackDto } from './dtos/feedback.create.dto';
import { UpdateFeedBackDto } from './dtos/feedback.update.dto';
import { FeedBackFilterType } from './interfaces/feedback.filter-type.interface';
import { FeedBackPaginationResponseType } from './interfaces/feedback.pagination-response-type.interface';
import { SORT_BY_FEEDBACK } from './constants/feedback.sort-by.constants';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class FeedbackService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(feedbackData: CreateFeedBackDto): Promise<feedback> {
    const user = await this.prismaService.user.findFirst({
      where: { id: feedbackData.user_id },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user does not exist',
      });
    }

    const product = await this.prismaService.product.findFirst({
      where: { id: feedbackData.product_id },
    });

    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }

    return await this.prismaService.feedback.create({
      data: { ...feedbackData },
    });
  }

  async update(id: number, feedbackData: UpdateFeedBackDto): Promise<feedback> {
    const feedback = await this.prismaService.feedback.findFirst({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'feedback does not exist',
      });
    }

    const user = await this.prismaService.user.findFirst({
      where: { id: feedbackData.user_id },
    });
    if (!user) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user does not exist',
      });
    }

    const product = await this.prismaService.product.findFirst({
      where: { id: feedbackData.product_id },
    });

    if (!product) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }

    return await this.prismaService.feedback.update({
      where: { id: id },
      data: {
        ...feedbackData,
        updated_at: new Date(),
      },
    });
  }

  async getList(
    filter: FeedBackFilterType,
  ): Promise<FeedBackPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    let order_by: 'asc' | 'desc' =
      filter.order_by?.toLowerCase() === 'desc' ? 'desc' : 'asc';

    const user_id: number = Number(filter.user_id);
    const product_id: number = Number(filter.product_id);
    const rating: number = Number(filter.rating);
    const whereConditions: any = {
      OR: [
        {
          comment: { contains: search },
        },
      ],
    };
    if (!isNaN(user_id)) {
      whereConditions.user_id = user_id;
    }

    if (!isNaN(product_id)) {
      whereConditions.product_id = product_id;
    }

    if (!isNaN(rating)) {
      whereConditions.rating = rating;
    }

    if (!SORT_BY_FEEDBACK.includes(sort_by)) {
      sort_by = 'created_at';
    }

    const feedbacks = await this.prismaService.feedback.findMany({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
    });
    const total = await this.prismaService.feedback.count({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
    });
    return {
      data: feedbacks,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }

  async getDetail(id: number): Promise<feedback> {
    const feedback = await this.prismaService.feedback.findFirst({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'feedback not found',
      });
    }
    return feedback;
  }

  async delete(id: number): Promise<DeleteResponse<feedback>> {
    const feedback = await this.prismaService.feedback.findFirst({
      where: { id: id },
    });
    if (!feedback) {
      throw new NotFoundException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'feedback not found',
      });
    }
    await this.prismaService.feedback.delete({
      where: { id: id },
    });
    return {
      message: 'delete feedback successful',
      data: feedback,
    };
  }
}
