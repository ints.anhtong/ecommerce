import { BadRequestException, Injectable, HttpStatus } from '@nestjs/common';
import { PrismaService } from 'src/prisma.service';
import { UpdateUserDto } from './dtos/user.update.dto';
import { user } from '@prisma/client';
import { UserPaginationResponseType } from './interfaces/user.pagination-response-type.interface';
import { UserFilterType } from './interfaces/user.filter-type.interface';
import { SORT_BY_USER } from './constants/user.sort-by.constant';
import { UserGetDetailResponseType } from './interfaces/user.get-detail-response';

@Injectable()
export class UserService {
  constructor(private readonly prismaService: PrismaService) {}

  async update(id: number, userData: UpdateUserDto): Promise<user> {
    const user = await this.prismaService.user.findFirst({
      where: { id: id },
    });
    if (!user) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'product does not exist',
      });
    }

    return await this.prismaService.user.update({
      where: { id: id },
      data: { ...userData, updated_at: new Date() },
    });
  }

  async getList(filter: UserFilterType): Promise<UserPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;
    let sort_by: string = filter.sort_by || 'created_at';
    let order_by: 'asc' | 'desc' =
      filter.order_by?.toLowerCase() === 'desc' ? 'desc' : 'asc';

    if (!SORT_BY_USER.includes(sort_by)) {
      sort_by = 'created_at';
    }
    const whereConditions: any = {
      OR: [
        {
          firstname: { contains: search },
        },
        { lastname: { contains: search } },
        { phone_number_or_email: { contains: search } },
        { address: { contains: search } },
      ],
    };
    const users = await this.prismaService.user.findMany({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
      select: {
        id: true,
        firstname: true,
        lastname: true,
        phone_number_or_email: true,
        address: true,
      },
    });
    const total = await this.prismaService.user.count({
      take: items_per_page,
      skip: skip,
      where: whereConditions,
      orderBy: { [sort_by]: order_by },
    });
    return {
      data: users,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }

  async getDetail(id: number): Promise<UserGetDetailResponseType> {
    const user = await this.prismaService.user.findFirst({
      where: { id: id },
    });
    if (!user) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'user not found',
      });
    }
    return {
      firstname: user.firstname,
      lastname: user.lastname,
      phone_number_or_email: user.phone_number_or_email,
      address: user.address,
    };
  }
}
