export interface UserGetDetailResponseType {
  firstname: string;
  lastname: string;
  phone_number_or_email: string;
  address: string;
}
