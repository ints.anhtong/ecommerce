import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsNotEmpty, IsOptional, MinLength } from 'class-validator';

export class UpdateUserDto {
  @ApiProperty({
    example: faker.person.firstName(),
    required: true,
  })
  @IsNotEmpty()
  @Type(() => String)
  @MinLength(2)
  firstname: string;

  @ApiProperty({
    example: faker.person.lastName(),
    required: true,
  })
  @IsNotEmpty()
  @MinLength(2)
  @Type(() => String)
  lastname: string;

  @ApiProperty({
    example: faker.location.streetAddress(),
    required: true,
  })
  @IsOptional()
  @MinLength(2)
  @Type(() => String)
  address: string;
}
