export const SORT_BY_USER: string[] = [
  'created_at',
  'updated_at',
  'firstname',
  'lastname',
  'phone_number_or_email',
  'address',
];
