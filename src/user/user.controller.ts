import {
  Body,
  Controller,
  Get,
  Param,
  ParseIntPipe,
  Put,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { ApiTags } from '@nestjs/swagger';
import { Roles } from 'src/auth/decorators/role.decorator';
import { UpdateUserDto } from './dtos/user.update.dto';
import { user } from '@prisma/client';
import { UserFilterType } from './interfaces/user.filter-type.interface';
import { UserPaginationResponseType } from './interfaces/user.pagination-response-type.interface';
import { UserGetDetailResponseType } from './interfaces/user.get-detail-response';
import {
  UserGetDetailDoc,
  UserGetListDoc,
  UserUpdateDoc,
} from './docs/user.doc';

@ApiTags('Users')
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {}

  @UserUpdateDoc()
  @Put(':id')
  @Roles('user', 'store', 'admin')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateUserDto,
  ): Promise<user> {
    return this.userService.update(id, body);
  }

  @UserGetListDoc()
  @Get()
  @Roles('admin')
  getAll(@Query() filter: UserFilterType): Promise<UserPaginationResponseType> {
    return this.userService.getList(filter);
  }

  @UserGetDetailDoc()
  @Get(':id')
  @Roles('user', 'store', 'admin')
  getDetail(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<UserGetDetailResponseType> {
    return this.userService.getDetail(id);
  }
}
