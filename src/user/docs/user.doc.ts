import { applyDecorators } from '@nestjs/common';
import { ApiOperation, ApiParam, ApiQuery, ApiResponse } from '@nestjs/swagger';
import {
    UserGetDetailSerialization,
  UserGetListSerialization,
  UserUpdateSerialization,
} from '../serializations/user.serialization';
import { faker } from '@faker-js/faker';

export function UserUpdateDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module user' }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserUpdateSerialization,
    }),
  );
}

export function UserGetListDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module product' }),
    ApiQuery({
      name: 'items_per_page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'page',
      example: faker.number.int(20),
      type: 'number',
    }),
    ApiQuery({
      name: 'search',
      example: 'meat',
      type: 'string',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserGetListSerialization,
    }),
  );
}

export function UserGetDetailDoc(): MethodDecorator {
  return applyDecorators(
    ApiOperation({ summary: 'module product' }),
    ApiParam({
      name: 'id',
      example: faker.number.int(50),
      type: 'number',
    }),
    ApiResponse({
      status: 200,
      description: 'Success',
      type: UserGetDetailSerialization,
    }),
  );
}
