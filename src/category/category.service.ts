import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { CreateCategoryDto } from './dto/category.create.dto';
import { category } from '@prisma/client';
import { PrismaService } from 'src/prisma.service';
import { UpdateCategoryDto } from './dto/category.update.dto';
import { CategoryFilterType } from './interfaces/category.filter-type.interface';
import { CategoryPaginationResponseType } from './interfaces/category.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';

@Injectable()
export class CategoryService {
  constructor(private readonly prismaService: PrismaService) {}

  async create(categoryData: CreateCategoryDto): Promise<category> {
    // check exists name category
    const category = await this.prismaService.category.findFirst({
      where: { name: categoryData.name },
    });
    if (category) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'category is exist',
      });
    }

    return await this.prismaService.category.create({
      data: { ...categoryData },
    });
  }

  async update(id: number, categoryData: UpdateCategoryDto): Promise<category> {
    // check exists name category
    const category = await this.prismaService.category.findFirst({
      where: { id: id },
    });

    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'category does not exist',
      });
    }
    const checkNameCategory = await this.prismaService.category.findFirst({
      where: { name: categoryData.name },
    });
    if (checkNameCategory) {
      throw new BadRequestException({
        statusCode: HttpStatus.BAD_REQUEST,
        message: 'category name is exist',
      });
    }
    return await this.prismaService.category.update({
      where: { id: id },
      data: {
        ...categoryData,
        updated_at: new Date(),
      },
    });
  }

  async getListCategory(
    filter: CategoryFilterType,
  ): Promise<CategoryPaginationResponseType> {
    const items_per_page: number = Number(filter.items_per_page) || 10;
    const page: number = filter.page || 1;
    const search: string = filter.search || '';
    const skip = page - 1 ? (page - 1) * items_per_page : 0;

    const categories = await this.prismaService.category.findMany({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          {
            name: { contains: search },
          },
          { description: { contains: search } },
        ],
      },
      orderBy: { created_at: 'desc' },
    });
    const total = await this.prismaService.category.count({
      take: items_per_page,
      skip: skip,
      where: {
        OR: [
          {
            name: { contains: search },
          },
          { description: { contains: search } },
        ],
      },
      orderBy: { created_at: 'desc' },
    });
    return {
      data: categories,
      total,
      currentPage: page,
      itemsPerPage: items_per_page,
    };
  }

  async getDetail(id: number): Promise<category> {
    const category = await this.prismaService.category.findFirst({
      where: { id: id },
    });
    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Category not found',
      });
    }
    return category;
  }

  async delete(id: number): Promise<DeleteResponse<category>> {
    const category = await this.prismaService.category.findFirst({
      where: { id: id },
    });
    if (!category) {
      throw new BadRequestException({
        statusCode: HttpStatus.NOT_FOUND,
        message: 'Category not found',
      });
    }
    await this.prismaService.category.delete({
      where: { id: id },
    });

    return {
      message: 'delete category successful',
      data: category,
    };
  }
}
