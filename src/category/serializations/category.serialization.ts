import { faker } from '@faker-js/faker';
import { ApiProperty } from '@nestjs/swagger';
import { CategoryPaginationResponseType } from '../interfaces/category.pagination-response-type.interface';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { category } from '@prisma/client';

export class CategoryCreateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.commerce.product(),
    required: true,
    nullable: false,
  })
  readonly name: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: null,
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
}

export class CategoryUpdateSerialization {
  @ApiProperty({
    example: faker.number.int(50),
    required: true,
    nullable: false,
  })
  readonly id: number;
  @ApiProperty({
    example: faker.commerce.product(),
    required: true,
    nullable: false,
  })
  readonly name: string;
  @ApiProperty({
    example: faker.commerce.productDescription(),
    required: true,
    nullable: false,
  })
  readonly description: string;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly created_at: Date;
  @ApiProperty({
    example: faker.date.birthdate(),
    required: true,
    nullable: false,
  })
  readonly updated_at: Date;
}

export class CategoryGetListSerialization
  implements CategoryPaginationResponseType
{
  @ApiProperty({
    type: [CategoryUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: CategoryUpdateSerialization[];
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  total: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  currentPage: number;
  @ApiProperty({
    example: faker.number.int(10),
    required: true,
    nullable: false,
  })
  itemsPerPage: number;
}

export class CategoryGetDetailSerialization extends CategoryUpdateSerialization {}

export class CategoryDeleteSerialization implements DeleteResponse<category> {
  @ApiProperty({
    example: 'delete category successful',
  })
  message: string;
  @ApiProperty({
    type: [CategoryUpdateSerialization],
    required: true,
    nullable: false,
  })
  data: CategoryUpdateSerialization;
}
