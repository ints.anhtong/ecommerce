import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  ParseIntPipe,
  Post,
  Put,
  Query,
} from '@nestjs/common';
import { CategoryService } from './category.service';
import { CreateCategoryDto } from './dto/category.create.dto';
import { category } from '@prisma/client';
import { UpdateCategoryDto } from './dto/category.update.dto';
import { CategoryFilterType } from './interfaces/category.filter-type.interface';
import { CategoryPaginationResponseType } from './interfaces/category.pagination-response-type.interface';
import { Roles } from 'src/auth/decorators/role.decorator';
import { DeleteResponse } from 'src/common/helper/interfaces/helper.response-delete';
import { ApiTags } from '@nestjs/swagger';
import {
  CategoryCreateDoc,
  CategoryDeleteDoc,
  CategoryGetDetailDoc,
  CategoryGetListDoc,
  CategoryUpdateDoc,
} from './docs/category.doc';

@ApiTags('Category')
@Controller('category')
export class CategoryController {
  constructor(private readonly categoryService: CategoryService) {}

  @CategoryCreateDoc()
  @Post()
  @Roles('admin', 'store')
  create(@Body() body: CreateCategoryDto): Promise<category> {
    return this.categoryService.create(body);
  }

  @CategoryUpdateDoc()
  @Put(':id')
  @Roles('admin', 'store')
  update(
    @Param('id', ParseIntPipe) id: number,
    @Body() body: UpdateCategoryDto,
  ): Promise<category> {
    return this.categoryService.update(id, body);
  }

  @CategoryGetListDoc()
  @Get()
  @Roles('admin', 'user', 'store')
  getList(
    @Query() query: CategoryFilterType,
  ): Promise<CategoryPaginationResponseType> {
    return this.categoryService.getListCategory(query);
  }

  @CategoryGetDetailDoc()
  @Roles('admin', 'user', 'store')
  @Get(':id')
  getDetail(@Param('id', ParseIntPipe) id: number): Promise<category> {
    return this.categoryService.getDetail(id);
  }

  @CategoryDeleteDoc()
  @Roles('admin')
  @Delete(':id')
  delete(
    @Param('id', ParseIntPipe) id: number,
  ): Promise<DeleteResponse<category>> {
    return this.categoryService.delete(id);
  }
}
