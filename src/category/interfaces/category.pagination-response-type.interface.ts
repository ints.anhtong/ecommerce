import { category } from '@prisma/client';

export interface CategoryPaginationResponseType {
  data: category[];
  total: number;
  currentPage: number;
  itemsPerPage: number;
}
