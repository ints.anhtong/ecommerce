export interface CategoryFilterType {
  items_per_page?: number;
  page?: number;
  search?: string;
}
